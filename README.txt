DB2 Drupal driver README(0.0.1-beta)
====================================
This Driver provides an interface to IBM DB2 DataServer Version 9.7 and above.

Installation
============
This section describes the steps to setup Drupal DB2 environment

	Download and setup a web server(for example: Apache)
	----------------------------------------------------
	  Set up an application server as per the configuration recommended by 
	  Drupal (http://drupal.org/requirements).

	Download and install DB2
	------------------------
	  Download DB2 version 9.7 from  
	  http://www-01.ibm.com/software/data/db2/express/download.html, 
	  use the resources available in the link to get started with DB2.
	
	Configuring php for db2
	-----------------------
	  Follow the steps below for respective platforms
	  Linux Platforms
		Download the latest pdo_ibm extension for db2 from 
			http://pecl.php.net/package/PDO_IBM 
		Install pdo_ibm issuing the following commands
			phpize --clean
			phpize
			./configure --with-pdo-ibm=/home/db2inst1/sqllib
			#sqllib directory will be present under your 
			#DB2 instance user's home
			make clean
			make
			make install 

	  Windows platform
                Use the php_pdo_ibm.dll shipped in your DB2 installation package
		(In case of requirement of different version of pdo_ibm.dll 
		 write to opendev@us.ibm.com,to get the appropriate dll ). 
		Place the php_pdo_ibm.dll in the php extensions directory

	  Common for all platforms
		Add the below line in php.ini  file 
			extension=php_pdo_ibm.dll.  
		Add the following line in php.ini file 
		to increase the maximum execution time
			max_execution_time = 500
		restart app server for above changes to take affect
		
	
	Adding DB2 support in Drupal
	----------------------------
          1. Get the DB2 Driver for Drupal

git clone git://git.drupal.org/sandbox/deepthivankadari/1201284.git db2_driver

          2. Copy the files under the  "db2_driver" folder 
	     (obtained from #1 above) to the location where 
	     you have downloaded Drupal, maintaining the 
	     directory structure of drupal.

	  Note: Files menu.inc, install.inc and system.module 
		should replace the ones in the core.

	
	Set up the database
	-------------------
	  Start DB2 and issue the following command to create a new database 
	  that will be used to install and configure drupal.

		db2 CREATE DATABASE drupal pagesize 32768

	  Note:Database pagesize of 32K is to be set.
		
	Install drupal
	--------------
	  Follow the drupal installation steps to set up your new drupal site.

Supported Versions
==================
	Drupal
	------
		version 6.20
	DB2
	---
		version 9.7
