<?php
//DB2 database driver interface code for  0.0.1-beta version.
/**
 * @file
 * Database interface code for DB2 database servers.
 */

include_once './includes/db2specificsqls.inc';
/**
 * @ingroup database
 * @{
 */

/**
 * Report database status.
 */
define('DB2_QUERY_REGEXP', '/(%d|%s|%%|%f|%b|%n|%p|%a)/');
function db_status_report() {
  $t = get_t();
  $version = db_version();

  $form['db2'] = array(
  'title' => $t('db2 database'),
  'value' => $version,
  );
  if (version_compare($version, DRUPAL_MINIMUM_DB2) < 0) {
  $form['db2']['severity'] = REQUIREMENT_ERROR;
  $form['db2']['description'] = $t('Your db2 Server, version' . $version . ', is too old. Drupal requires at least db2 %version.', array('%version' => DRUPAL_MINIMUM_DB2));
 }

  return $form;
}

/**
 * Returns the version of the database server currently in use.
 *
 * @return Database server version
 */
function db_version() {
  $result = db_result(db_query("SELECT SERVICE_LEVEL FROM TABLE (SYSPROC.ENV_GET_INST_INFO()) AS INSTANCEINFO"));
  preg_match('/\s*v([0-9.]+)\s*/', $result, $version);
  return array_pop($version);
}

/**
 * Initialize a database connection.
 */
function db_connect($url) {
// Should check if PDO_IBM is present (How?)
  $url = parse_url($url);
  $conn_string = 'ibm:';
  // Decode url-encoded information in the db connection string
  if (isset($url['host'])) {
		$conn_string .= ' hostname='. urldecode($url['host']) . ';';
	}
  if (isset($url['path'])) {
		$conn_string .= ' database='. substr(urldecode($url['path']), 1) . ';';
	}
  if (isset($url['port'])) {
		$conn_string .= ' port='. urldecode($url['port']) . ';';
	}
  $conn_string .= ' protocol=TCPIP; EnableScrollableCursors=1;';
  $db_url = $conn_string;
  $connection = null;
  try {
		$connection = new PDO($conn_string, urldecode($url['user']), urldecode($url['pass']) );
		$connection->setAttribute(PDO::ATTR_CASE, PDO::CASE_LOWER);
		$connection->setAttribute(PDO::ATTR_STRINGIFY_FETCHES, TRUE);
		$connection->setAttribute(PDO::ATTR_PERSISTENT, TRUE);
      } 
  catch (PDOException $e) {
		_db_error_page($e->getMessage());
		return FALSE;
	}
  return $connection;
}
/**
 * Runs a basic query in the active database.
 *
 * User-supplied arguments to the query should be passed in as separate
 * parameters so that they can be properly escaped to avoid SQL injection
 * attacks.
 *
 * @param $query
 *   A string containing an SQL query.
 * @param ...
 *   A variable number of arguments which are substituted into the query
 *   using printf() syntax. Instead of a variable number of query arguments,
 *   you may also pass a single array containing the query arguments.
 *
 *   Valid %-modifiers are: %s, %d, %f, %b (binary data, do not enclose
 *   in '') and %%.
 *
 *   NOTE: using this syntax will cast NULL and FALSE values to decimal 0,
 *   and TRUE values to decimal 1.
 *
 * @return
 *   A database query result resource, or FALSE if the query was not
 *   executed correctly.
 */

function db_query($query) {
  global $blob_db2;
  $blob_db2 = array();
  global $marker_values;
  $marker_values = array();
  $args = func_get_args(); 
  array_shift($args); 	
  if (isset($args[0]) and is_array($args[0])) { // 'All arguments in one array' syntax
		$args = $args[0];
	} 
  if (db2_sql_skip($query) === TRUE)
	  return TRUE;

  if (preg_match("/^TRUNCATE TABLE/i",$query)) {
  $query = $query .' IMMEDIATE';
	}
  $query_clob = db2_hashlist($query);

  if($query_clob === FALSE) {
	  $query_match=db2_pmarker_normalexecute($query);

  if ($query_match === TRUE) {	
          $query = db_prefix_tables($query);
          _db_query_callback($args, TRUE);	
          $query = preg_replace_callback(DB_QUERY_REGEXP, '_db_query_callback', $query);
        }

  else {
         $query = $query_match;
         $query = db_prefix_tables($query);
         $query = db_placeholder_string($query);
         db2_db_query_callback($args, TRUE);	
         $query = preg_replace_callback(DB2_QUERY_REGEXP, 'db2_db_query_callback', $query);
      }
  }

  else {
         $query = $query_clob;
         $query = db_prefix_tables($query);
         $query = db_placeholder_string($query);
         db2_db_query_callback($args, TRUE);	
         $query = preg_replace_callback(DB2_QUERY_REGEXP, 'db2_db_query_callback', $query);
  }
   return _db_query($query);
}

function db2_hashlist($key) {	
  global $db2_hash_queries;	
  foreach ($db2_hash_queries as $db2_key => $db2_map_string) {
  if (strcmp($key,$db2_key) == 0) {
                     $sql_db2 = $db2_map_string;
                     return $sql_db2; 
                }
  }	
  $sql_db2 = db2_distinct_removal($key);
  if($sql_db2 === FALSE)
  return FALSE;	
  else
  return $sql_db2;	
}


function db2_distinct_removal($key) {
  if( preg_match("/^SELECT DISTINCT b.* FROM {blocks} b LEFT JOIN {blocks_roles} r.*%d(,%d)*.*/",$key)) {
  $key=str_replace("DISTINCT","",$key);
  return $key;
  }
  else
  return FALSE;
}

function db2_pmarker_normalexecute($mkey) {
  global $marker_sqls;		
  foreach ($marker_sqls as $db2_marker_sqls) {
  if (strcmp($mkey,$db2_marker_sqls) == 0) {
  return TRUE;		            
    }
  }
  $mkey=db2_pmarker_normalexecute_ext($mkey);
  if( $mkey===TRUE)
  return TRUE;
  else
  return $mkey;
}

function db2_pmarker_normalexecute_ext($mkey) {
  if( preg_match("/^SELECT.*{filter_formats} WHERE roles LIKE '%%,%d,%%'( OR roles LIKE '%%,%d,%%')*.*/",$mkey))
  return TRUE;
  else
  return $mkey;
}
function db2_sql_skip($query) {	
  global $skip_sqls;
  foreach ($skip_sqls as $db2_skip_sqls) {
  if (strcmp($query,$db2_skip_sqls) == 0) {
  return TRUE;	            
    }
  }	
  return FALSE;
}

function db_placeholder_string($query) {
  $query=preg_replace('/\'%s\'/i','%s',$query);
  $query=preg_replace('/\'\%\%\%s\%\%\'/i','%p%s%a',$query);
  $query=preg_replace('/\'\%\%\%s\'/i','%p%s',$query);
  $query=preg_replace('/\'%s\%\%\'/i','%s%a',$query);
  return $query;
}

function db_encode_numeric($data) {
  global $marker_values;	
  $size=count($marker_values);
  $marker_values[$size]=$data;
  $param_marker='?';
  return $param_marker;      
}

function db_encode_string($data) {
  global $marker_values,$marker_flag; 	
  if($marker_flag) {   
    $size=count($marker_values);
    $size=$size-1;
    $marker_values[$size]=$marker_values[$size].$data; 
    $marker_flag=0;          
   }
	  
   else {
    $size=count($marker_values);
    $marker_values[$size]=$data;
   }
  $param_marker='?';
  return $param_marker;      
}
/**
 * Helper function for db_query().
 */
function db2_db_query_callback($match, $init = FALSE) {
    global $marker_values,$marker_flag;
    static $args = NULL;
    if ($init) {
                 $args = $match;
                 return;
               }

    switch ($match[1]) {
    case '%d': // We must use type casting to int to convert FALSE/NULL/(TRUE?)
        $value = array_shift($args);
        // Do we need special bigint handling?
        if ($value > PHP_INT_MAX) {
        $precision = ini_get('precision');
        @ini_set('precision', 16);
        $value = sprintf('%.0f', $value);
        @ini_set('precision', $precision);
      }
      else {
              $value = (int) $value;
           }
      // We don't need db_escape_string as numbers are db-safe.
      //return $value;
       return db_encode_numeric($value); 
    case '%s':
	    $db2_args=array_shift($args);
	    if($db2_args===NULL){
		    $db2_args='';
	    } 
	    return db_encode_string($db2_args);
    case '%n':
      // Numeric values have arbitrary precision, so can't be treated as float.
      // is_numeric() allows hex values (0xFF), but they are not valid.
      $value = trim(array_shift($args));
      $value=is_numeric($value) && !preg_match('/x/i', $value) ? $value : '0';
      return db_encode_numeric($value);
    
    case '%p':
	    $marker_flag=1;        
            $value='%';
	    $size=count($marker_values);
	    $marker_values[$size]=$value;
	    return '';	
    case '%a':
	    $value='%';
	    $size=count($marker_values);
	    $size=$size-1;
	    $marker_values[$size]=$marker_values[$size].$value;
	    return ''; 
                
    case '%%':
      return '%';
    case '%f':
      //return (float) array_shift($args);
	    $float_value=(float) array_shift($args);
	    return db_encode_numeric($float_value);
    case '%b': // binary data
     return db_encode_numeric(array_shift($args));
  }
}



/**
 * Helper function for db_query().
 */

function _db_query($query, $debug = 0) {	
	
  global $blob_db2;
  global $active_db, $last_result, $queries, $marker_values, $affectedrows;	 
  if (variable_get('dev_query',0)) {
      list($usec, $sec) = explode(' ', microtime());
      $timer = (float)$usec + (float)$sec;
    }	
   $last_result = $active_db->prepare($query);
   if($last_result == FALSE) {
		return;
   }	
   $result_db2=$last_result->execute($marker_values);
   $affectedrows=$last_result->rowCount();
   $blob_db2=array();
   $marker_values=array(); 
   if (variable_get('dev_query', 0)) {
                $bt = debug_backtrace();
		$query = $bt[2]['function'] ."\n". $query;
		list($usec, $sec) = explode(' ', microtime());
		$stop = (float)$usec + (float)$sec;
		$diff = $stop - $timer;
		$queries[] = array($query, $diff);
	}

  if ($debug) {
	      print '<p>query error code: '. $query .'<br />error:'. $active_db->errorCode() .'</p>';
	      print '<p>query error information: '. $query .'<br />error:'. $active_db->errorInfo() .'</p>';

	  }
  if ($result_db2 !== FALSE) {
                            return $last_result;
	                    }
	// Indicate to drupal_error_handler that this is a database error.
  else {
	 ${DB_ERROR} = TRUE;
	  //trigger_error(check_plain(print_r($active_db->errorInfo()) ."\nquery: ". $query), E_USER_WARNING);
	  $error_msg =  $active_db->errorInfo();
	  $error_msg = implode(" ",$error_msg);
	  trigger_error(check_plain($error_msg."\nquery:". $query), E_USER_WARNING);
	  return FALSE;
	 }
}

// Get max uid from users table
function db_get_max_uid() {
  $result = _db_query("SELECT MAX(uid) from users");
  $id = $result->fetch();
  return $id['(max)'];
}
/**
 * Fetch one result row from the previous query as an object.
 *
 * @param $result
 *   A database query result resource, as returned from db_query().
 * @return
 *   An object representing the next row of the result, or FALSE. The attributes
 *   of this object are the table fields selected by the query.
 */
function db_fetch_object($result) {
  if ($result) {	
              $object= $result->fetchObject();
	      return isset($object) ? $object : FALSE;
	    }
}

/**
 * Fetch one result row from the previous query as an array.
 *
 * @param $result
 *   A database query result resource, as returned from db_query().
 * @return
 *   An associative array representing the next row of the result, or FALSE.
 *   The keys of this object are the names of the table fields selected by the
 *   query, and the values are the field values for this result row.
 */
function db_fetch_array($result) {	
  if ($result) {
	      return $result->fetch(PDO::FETCH_ASSOC);
            }
}
/**
 * Return an individual result field from the previous query.
 *
 * Only use this function if exactly one field is being selected; otherwise,
 * use db_fetch_object() or db_fetch_array().
 *
 * @param $result
 *   A database query result resource, as returned from db_query().
 * @return
 *   The resulting field or FALSE.
 */
function db_result($result) {
  if ($result ) {
                if ($array = $result->fetch(PDO::FETCH_NUM) ) {
		return $array[0];
	  }
    }
	return FALSE;
}

/**
 * Determine whether the previous query caused an error.
 */
function db_error() {
  global $last_result, $active_db;
  if ($last_result instanceof PDOStatement) {
		          return $active_db->errorCode();
	                }
	return(0);
}

/**
 * Returns the last insert id. This function is thread safe.
 *
 * @param $table
 *   The name of the table you inserted into.
 * @param $field
 *   The name of the autoincrement field.
 */
function db_last_insert_id($table, $field) {
  global $active_db;
  return $active_db->lastInsertId();
}

/**
 * Determine the number of rows changed by the preceding query.
 */
function db_affected_rows() {
  global $affectedrows;
  return empty($affectedrows) ? 0 : $affectedrows;
}

/**
 * Runs a limited-range query in the active database.
 *
 * Use this as a substitute for db_query() when a subset of the query
 * is to be returned.
 * User-supplied arguments to the query should be passed in as separate
 * parameters so that they can be properly escaped to avoid SQL injection
 * attacks.
 *
 * @param $query
 *   A string containing an SQL query.
 * @param ...
 *   A variable number of arguments which are substituted into the query
 *   using printf() syntax. Instead of a variable number of query arguments,
 *   you may also pass a single array containing the query arguments.
 *   Valid %-modifiers are: %s, %d, %f, %b (binary data, do not enclose
 *   in '') and %%.
 *
 *   NOTE: using this syntax will cast NULL and FALSE values to decimal 0,
 *   and TRUE values to decimal 1.
 *
 * @param $from
 *   The first result row to return.
 * @param $count
 *   The maximum number of result rows to return.
 * @return
 *   A database query result resource, or FALSE if the query was not executed
 *   correctly.
 */
function db_query_range($query) {
  $args = func_get_args();	
  $count = array_pop($args);
  $from = array_pop($args);
  $last_record=$count+$from;
  array_shift($args);
  if (isset($args[0]) and is_array($args[0])) { // 'All arguments in one array' syntax
		$args = $args[0];
	}	
  $query_clob=db2_hashlist($query);
  if ($query_clob === FALSE) {	
                $query_match=db2_pmarker_normalexecute($query);	
		if ($query_match === TRUE) {
			                  $query = db_prefix_tables($query);
			                   _db_query_callback($args, TRUE);	
	                                  $query = preg_replace_callback(DB_QUERY_REGEXP, '_db_query_callback', $query);
		                        }
		 else {
		      $query=$query_match;
		      $query = db_prefix_tables($query);
	  	      $query= db_placeholder_string($query);
	              db2_db_query_callback($args, TRUE);	
	              $query = preg_replace_callback(DB2_QUERY_REGEXP, 'db2_db_query_callback', $query);
		    }
    }
  else {
       $query=$query_clob;
       $query = db_prefix_tables($query);
       $query= db_placeholder_string($query);
       db2_db_query_callback($args, TRUE);	
       $query = preg_replace_callback(DB2_QUERY_REGEXP, 'db2_db_query_callback', $query);
     }
  $query = preg_replace("/SELECT/i", "SELECT O.* FROM (SELECT I.*, ROW_NUMBER() OVER () sys_row_num FROM (SELECT", $query);
  $query .= ") AS I) AS O WHERE sys_row_num BETWEEN $from AND $last_record";  	
  return _db_query($query);
}	
/**
 * Runs a SELECT query and stores its results in a temporary table.
 *
 * Use this as a substitute for db_query() when the results need to stored
 * in a temporary table. Temporary tables exist for the duration of the page
 * request.
 * User-supplied arguments to the query should be passed in as separate parameters
 * so that they can be properly escaped to avoid SQL injection attacks.
 *
 * Note that if you need to know how many results were returned, you should do
 * a SELECT COUNT(*) on the temporary table afterwards. db_affected_rows() does
 * not give consistent result across different database types in this case.
 *
 * @param $query
 *   A string containing a normal SELECT SQL query.
 * @param ...
 *   A variable number of arguments which are substituted into the query
 *   using printf() syntax. The query arguments can be enclosed in one
 *   array instead.
 *   Valid %-modifiers are: %s, %d, %f, %b (binary data, do not enclose
 *   in '') and %%.
 *
 *   NOTE: using this syntax will cast NULL and FALSE values to decimal 0,
 *   and TRUE values to decimal 1.
 *
 * @param $table
 *   The name of the temporary table to select into. This name will not be
 *   prefixed as there is no risk of collision.
 * @return
 *   A database query result resource, or FALSE if the query was not executed
 *   correctly.
 */
// TODO: This is not implemented yet. Problems with the temp table definition
// We could execute the query and get the definition from the result set
//
function db_query_temporary($query) {
  $args = func_get_args();
  $tablename = array_pop($args);
  array_shift($args);
  $query = preg_replace('/^SELECT/i', 'CREATE TEMPORARY TABLE '. $tablename .' AS SELECT', db_prefix_tables($query));
  if (isset($args[0]) and is_array($args[0])) { // 'All arguments in one array' syntax
    $args = $args[0];
                                        }
  db2_db_query_callback($args, TRUE);
  $query = preg_replace_callback(DB_QUERY_REGEXP, 'db2_db_query_callback', $query);
  return _db_query($query);
}

/**
 * Returns a properly formatted Binary Large OBject value.
 * In case of PostgreSQL encodes data for insert into bytea field.
 *
 * @param $data
 *   Data to encode.
 * @return
 *  Encoded data.
 */
function db_encode_blob($data) {
  global $blob_db2; 
  $size=count($blob_db2);
  $blob_db2[$size]=$data;
  $blob_param_marker='?';
  return $blob_param_marker;      
}
/**
 * Returns text from a Binary Large OBject value.
 * In case of PostgreSQL decodes data after select from bytea field.
 *
 * @param $data
 *   Data to decode.
 * @return
 *  Decoded data.
 */

function db_decode_blob($data) {
  return $data;
}

/**
 * Prepare user input for use in a database query, preventing SQL injection attacks.
 * Note: This function requires PostgreSQL 7.2 or later.
 */
function db_escape_string($text) {
  $text = preg_replace("/'/", "''", $text);
  global $active_db;
  return $text; 
}

function db_trim_front_and_back($text) {
  $text = substr($text, 0, -1);		// chop the end '
  $text = substr($text, 1);			// chop the front '
  return $text;
}
/**
 * Lock a table.
 * This function automatically starts a transaction.
 */
function db_lock_table($table) {
  global $active_db;
  //$active_db->setAttribute(PDO::ATTR_AUTOCOMMIT, false);
  db_query('LOCK TABLE {'. db_escape_table($table) .'} IN EXCLUSIVE MODE');
}

/**
 * Unlock all locked tables.
 * This function automatically commits a transaction.
 */
function db_unlock_tables() {
  global $active_db;
  db_query('COMMIT');
  //$active_db->setAttribute(PDO::ATTR_AUTOCOMMIT, true);
}

function db2_begin_transaction() {
  global $active_db;
  $active_db->setAttribute(PDO::ATTR_AUTOCOMMIT, false);	
}


function db2_end_transaction($commit = FALSE) {
  global $active_db;	
  if ($commit)	
  db_query('COMMIT');
  else if(!$commit)
  db_query('ROLLBACK');
  $active_db->setAttribute(PDO::ATTR_AUTOCOMMIT, true);
}

/**
 * Check if a table exists.
 */
function db_table_exists($table) {
  $table=strtoupper($table);
  return db_result(db_query("SELECT COUNT(table_name) FROM sysibm.tables WHERE table_name = '{". db_escape_table($table) ."}'"));
}

/**
 * Check if a column exists in the given table.
 */
function db_column_exists($table, $column) {
  $table=strtoupper($table);
  $column=strtoupper($column);
  return db_result(db_query("SELECT COUNT(column_name) FROM sysibm.columns WHERE table_name = '{". db_escape_table($table) ."}' AND column_name = " . $column));
}
/**
 * @} End of "ingroup database".
 */

/**
 * @ingroup schemaapi
 * @{
 */

/**
 * This maps a generic data type in combination with its data size
 * to the engine-specific data type.
 */
function db_type_map() {
	// Put :normal last so it gets preserved by array_flip.  This makes
	// it much easier for modules (such as schema.module) to map
	// database types back into schema types.
  $map = array(
    'varchar:normal'  => 'VARCHAR',
	
    'text:tiny'       => 'VARCHAR(4000)',
    'text:small'      => 'VARCHAR(4000)',
    'text:medium'     => 'CLOB',
    'text:big'        => 'CLOB',
    'text:normal'     => 'CLOB',

    'int:tiny'        => 'SMALLINT',
    'int:small'       => 'SMALLINT',
    'int:medium'      => 'INTEGER',
    'int:big'         => 'BIGINT',
    'int:normal'      => 'INTEGER',

    'float:tiny'      => 'REAL',
    'float:small'     => 'REAL',
    'float:medium'    => 'REAL',
    'float:big'       => 'DOUBLE',
    'float:normal'    => 'REAL',

    'numeric:normal'  => 'DECIMAL',

    'blob:big'        => 'BLOB(1G)',
    'blob:normal'     => 'BLOB(1G)',

    'clob:big'        => 'CLOB(1G)',
    'clob:normal'     => 'CLOB(1G)',

    'datetime:normal' => 'TIMESTAMP',

    'serial:tiny'     => 'INTEGER',
    'serial:small'    => 'INTEGER',
    'serial:medium'   => 'INTEGER',
    'serial:big'      => 'BIGINT',
    'serial:normal'   => 'INTEGER', 
	);
	return $map;
}

/**
 * Generate SQL to create a new table from a Drupal schema definition.
 *
 * @param $name
 *   The name of the table to create.
 * @param $table
 *   A Schema API table definition array.
 * @return
 *   An array of SQL statements to create the table.
 */
function db_create_table_sql($name, $table) {
  global $createtable_alternatesql,$createunique_index;
  $db2_createflag=0;
  $sql_fields = array();
   foreach ($table['fields'] as $field_name => $field) {
		$sql_fields[] = _db_create_field_sql($field_name, _db_process_field($field));
	}
   $sql_keys = array();
  
  if (isset($table['primary key']) && is_array($table['primary key'])) {
		$sql_keys[] = 'PRIMARY KEY ('. implode(', ', $table['primary key']) .')';
	}

  if (isset($table['unique keys']) && is_array($table['unique keys'])) {
		foreach ($table['unique keys'] as $key_name => $key) {
		$sql_keys[] = ' UNIQUE ('. implode(', ', $key) .')';
	   }
   }

  $sql = "CREATE TABLE {". $name ."} (";
  $sql .= implode(",", $sql_fields);

  if (count($sql_keys) > 0) {
	         $sql .= ",";
	  }
  $sql .= implode(",", $sql_keys);
  $sql .= " )";	
  foreach($createtable_alternatesql as $db2_create_key => $db2_create_alternate) {
  if (strcmp($sql,$db2_create_key) == 0) {	
    $db2_createflag=1;
    $sql_create = $db2_create_alternate;
    $statements[] = $sql_create;
    $sql_db2 = $createunique_index[$sql];
    $statements[] = $sql_db2;
    break;	
    }	
  }
  if (!$db2_createflag) 
     $statements[]=$sql;
  if (isset($table['indexes']) && is_array($table['indexes'])) {
		foreach ($table['indexes'] as $key_name => $key) {
		$statements[] = _db_create_index_sql($name, $key_name, $key, $table);
		}
	}
  return $statements;
}

function _db_create_index_sql($table, $name, $fields, $tabdef) {
  $query = 'CREATE INDEX {'. $table .'}_'. $name .'_idx ON {'. $table .'} (';	
  $query .= _db_create_key_sql($fields) .')';
  return $query;
}

// We need to make sure we don't reach the index size limit
// so we pass the table definition to get column sizes
// TODO:  right now we stop when we can't fit anymore
// We could ask checking if there is a dbspace with bigger pagesize
// to accomodate larger keys: maxKeySize= ((pagesize-93) / 5) -1
// in db2 max index key size=indexpagesize/4

function _db_create_key_sql($fields) {
  $ret = array();
  foreach ($fields as $field) {
  if (is_array($field)) {
  $ret[] = $field[0];
          }
  else {
      $ret[] = $field;
     }
  }
  return implode(', ', $ret);
}


function _db_create_keys(&$ret, $table, $new_keys) {
  if (isset($new_keys['primary key'])) {
		db_add_primary_key($ret, $table, $new_keys['primary key']);
	}
  if (isset($new_keys['unique keys'])) {
		foreach ($new_keys['unique keys'] as $name => $fields) {
		db_add_unique_key($ret, $table, $name, $fields);
	   }
	}
  if (isset($new_keys['indexes'])) {
		foreach ($new_keys['indexes'] as $name => $fields) {
			db_add_index($ret, $table, $name, $fields);
	   }
	}
}

/**
 * Set database-engine specific properties for a field.
 *
 * @param $field
 *   A field description array, as specified in the schema documentation.
 */
function _db_process_field($field) {
  if (!isset($field['size'])) {
  $field['size'] = 'normal';
  }
  // Set the correct database-engine specific datatype.
  if (!isset($field['db2_type'])) {
  $map = db_type_map();
  $field['db2_type'] = $map[$field['type'] .':'. $field['size']];
 }
 if ($field['type'] == 'serial') {
          unset($field['not null']);
     }
  return $field;
}

/**
 * Create an SQL string for a field to be used in table creation or alteration.
 *
 * Before passing a field out of a schema definition into this function it has
 * to be processed by _db_process_field().
 *
 * @param $name
 *    Name of the field.
 * @param $spec
 *    The field specification, as per the schema data structure format.
 */
// TODO: Should probably add UNIQUE to serial columns

function _db_create_field_sql($name, $spec) {
  $sql = $name .' '. $spec['db2_type'];

  if (($spec['type'] == 'blob') || ($spec['type'] == 'clob')) {
     unset($spec['not null']);
  }

  if (!empty($spec['length'])) {
    $sql .= '('. $spec['length'] .')';
  }
  elseif (isset($spec['precision']) && isset($spec['scale'])) {
    $sql .= '('. $spec['scale'] .', '. $spec['precision'] .')';
  }

  if (isset($spec['not null']) && $spec['not null']) {
    $sql .= ' NOT NULL';
  }

  if (isset($spec['default'])) {
    $default = is_string($spec['default']) ? "'". $spec['default'] ."'" : $spec['default'];
    $sql .= " DEFAULT $default";
  }

  if (!empty($spec['unsigned'])) {
    $sql .= ' CHECK (' . $name . ' >= 0)';
  }

  if ($spec['type'] == 'serial') {
    $sql .= ' GENERATED BY DEFAULT AS IDENTITY (START WITH +1 INCREMENT BY +1 NO CYCLE NO CACHE ORDER)';
  }
  return $sql;
}
/**
 * Rename a table.
 *
 * @param $ret
 *   Array to which query results will be added.
 * @param $table
 *   The table to be renamed.
 * @param $new_name
 *   The new name for the table.
 */
function db_rename_table(&$ret, $table, $new_name) {
  $ret[] = update_sql('RENAME TABLE {'. $table .'} TO {'. $new_name .'}');
}

/**
 * Drop a table.
 *
 * @param $ret
 *   Array to which query results will be added.
 * @param $table
 *   The table to be dropped.
 */
function db_drop_table(&$ret, $table) {
  $ret[] = update_sql('DROP TABLE {'. $table .'}');
}

/**
 * Add a new field to a table.
 *
 * @param $ret
 *   Array to which query results will be added.
 * @param $table
 *   Name of the table to be altered.
 * @param $field
 *   Name of the field to be added.
 * @param $spec
 *   The field specification array, as taken from a schema definition.
 *   The specification may also contain the key 'initial', the newly
 *   created field will be set to the value of the key in all rows.
 *   This is most useful for creating NOT NULL columns with no default
 *   value in existing tables.
 * @param $keys_new
 *   Optional keys and indexes specification to be created on the
 *   table along with adding the field. The format is the same as a
 *   table specification but without the 'fields' element.  If you are
 *   adding a type 'serial' field, you MUST specify at least one key
 *   or index including it in this array. @see db_change_field for more
 *   explanation why.
 */
function db_add_field(&$ret, $table, $field, $spec, $new_keys = array()) {
  $fixnull = FALSE;
  if (!empty($spec['not null']) && !isset($spec['default'])) {
		$fixnull = TRUE;
		$spec['not null'] = FALSE;
	}
  $query = 'ALTER TABLE {'. $table .'} ADD COLUMN ';
  $query .= _db_create_field_sql($field, _db_process_field($spec));
  $ret[] = update_sql($query);
  if (isset($spec['initial'])) {
		// All this because update_sql does not support %-placeholders.
		$sql = 'UPDATE {'. $table .'} SET '. $field .' = '. db_type_placeholder($spec['type']);
		$result = db_query($sql, $spec['initial']);
		$ret[] = array('success' => $result !== FALSE, 'query' => check_plain($sql .' ('. $spec['initial'] .')'));
	}
  if ($fixnull) {
	        $ret[] = update_sql("ALTER TABLE {". $table ."} ALTER COLUMN $field SET NOT NULL");
              }
  if (isset($new_keys)) {
		_db_create_keys($ret, $table, $new_keys);
	      }
}

/**
 * Drop a field.
 *
 * @param $ret
 *   Array to which query results will be added.
 * @param $table
 *   The table to be altered.
 * @param $field
 *   The field to be dropped.
 */
function db_drop_field(&$ret, $table, $field) {
  $ret[] = update_sql('ALTER TABLE {'. $table .'} DROP '. $field);
}

/**
 * Set the default value for a field.
 *
 * @param $ret
 *   Array to which query results will be added.
 * @param $table
 *   The table to be altered.
 * @param $field
 *   The field to be altered.
 * @param $default
 *   Default value to be set. NULL for 'default NULL'.
 */
function db_field_set_default(&$ret, $table, $field, $default) {
  if ($default === NULL) {
		 $default = 'NULL';
	      }
  else {
	$default = is_string($default) ? "'$default'" : $default;
       }

  $ret[] = update_sql('ALTER TABLE {'. $table .'} ALTER COLUMN '. $field .' SET DEFAULT '. $default);
}

/**
 * Set a field to have no default value.
 *
 * @param $ret
 *   Array to which query results will be added.
 * @param $table
 *   The table to be altered.
 * @param $field
 *   The field to be altered.
 */
function db_field_set_no_default(&$ret, $table, $field) {
  $ret[] = update_sql('ALTER TABLE {'. $table .'} ALTER COLUMN '. $field .' DROP DEFAULT');
}

/**
 * Add a primary key.
 *
 * @param $ret
 *   Array to which query results will be added.
 * @param $table
 *   The table to be altered.
 * @param $fields
 *   Fields for the primary key.
 */
function db_add_primary_key(&$ret, $table, $fields) {
  $ret[] = update_sql('ALTER TABLE {'. $table .'} ADD CONSTRAINT pk_{' . $table . '} PRIMARY KEY (' . implode(',', $fields) . ')');
}

/**
 * Drop the primary key.
 *
 * @param $ret
 *   Array to which query results will be added.
 * @param $table
 *   The table to be altered.
 */
function db_drop_primary_key(&$ret, $table) {
  $ret[] = update_sql('ALTER TABLE {'. $table .'} DROP PRIMARY KEY');
}

/**
 * Add a unique key.
 *
 * @param $ret
 *   Array to which query results will be added.
 * @param $table
 *   The table to be altered.
 * @param $name
 *   The name of the key.
 * @param $fields
 *   An array of field names.
 */
function db_add_unique_key(&$ret, $table, $name, $fields) {
  $name = '{'. $table .'}_'. $name .'_key';
  $ret[] = update_sql('ALTER TABLE {' . $table . '} ADD CONSTRAINT ' . $name . ' UNIQUE (' . implode(',', $fields) . ')');
}

/**
 * Drop a unique key.
 *
 * @param $ret
 *   Array to which query results will be added.
 * @param $table
 *   The table to be altered.
 * @param $name
 *   The name of the key.
 */
function db_drop_unique_key(&$ret, $table, $name) {
  $name = '{'. $table .'}_'. $name .'_key';
  $ret[] = update_sql('ALTER TABLE {'. $table .'} DROP UNIQUE '. $name);
}


/**
 * Add an index.
 *
 * @param $ret
 *   Array to which query results will be added.
 * @param $table
 *   The table to be altered.
 * @param $name
 *   The name of the index.
 * @param $fields
 *   An array of field names.
 */
function db_add_index(&$ret, $table, $name, $fields) {
  $ret[] = update_sql(_db_create_index_sql($table, $name, $fields, NULL));
}

/**
 * Drop an index.
 *
 * @param $ret
 *   Array to which query results will be added.
 * @param $table
 * db_drop_primary_key($ret, 'foo');
 * db_change_field($ret, 'foo', 'bar', 'bar',
 *   array('type' => 'serial', 'not null' => TRUE),
 *   array('primary key' => array('bar')));
 * @endcode
 *
 * The reasons for this are due to the different database engines:
 *
 * On PostgreSQL, changing a field definition involves adding a new field
 * and dropping an old one which* causes any indices, primary keys and
 * sequences (from serial-type fields) that use the changed field to be dropped.
 *
 * On MySQL, all type 'serial' fields must be part of at least one key
 * or index as soon as they are created.  You cannot use
 * db_add_{primary_key,unique_key,index}() for this purpose because
 * the ALTER TABLE command will fail to add the column without a key
 * or index specification.  The solution is to use the optional
 * $new_keys argument to create the key or index at the same time as
 * field.
 *
 * You could use db_add_{primary_key,unique_key,index}() in all cases
 * unless you are converting a field to be type serial. You can use
 * the $new_keys argument in all cases.
 *
 * @param $ret
 *   Array to which query results will be added.
 * @param $table
 *   Name of the table.
 * @param $field
 *   Name of the field to change.
 * @param $field_new
 *   New name for the field (set to the same as $field if you don't want to change the name).
 * @param $spec
 *   The field specification for the new field.
 * @param $new_keys
 *   Optional keys and indexes specification to be created on the
 *   table along with changing the field. The format is the same as a
 *   table specification but without the 'fields' element.
 */
function db_change_field(&$ret, $table, $field, $field_new, $spec, $new_keys = array()) {
  $notSame = strcmp($field, $field_new);
  if ($notSame) {
		//$ret[] = update_sql('RENAME COLUMN {'. $table .'}.'. $field .' TO '. $field_new);
		$ret[] = update_sql('ALTER TABLE { '.$table.'} RENAME COLUMN '.$field .' TO '. $field_new);
	  }
  $spec=_db_process_field($spec);
  $sql = $spec['db2_type'];
  if (($spec['type'] == 'blob') || ($spec['type'] == 'clob')) {
                            unset($spec['not null']);
        }

  if (!empty($spec['length'])) {
                            $sql .= '('. $spec['length'] .')';
                        }

  elseif (isset($spec['precision']) && isset($spec['scale'])) {
  $sql .= '('. $spec['scale'] .', '. $spec['precision'] .')';
  }

  $ret[] = update_sql('ALTER TABLE { '.$table.'} ALTER COLUMN ' . $field_new.' SET DATA TYPE '. $sql);

  if (isset($spec['not null']) && $spec['not null']) 
  $ret[] = update_sql('ALTER TABLE { '.$table.'} ALTER COLUMN '. $field_new.' SET NOT NULL');

  if (isset($spec['default'])) {
                           $default = is_string($spec['default']) ? "'". $spec['default'] ."'" : $spec['default'];
                           $ret[] = update_sql('ALTER TABLE {'. $table .'} ALTER COLUMN '. $field_new .' SET DEFAULT '. $default);
	                }  

  if (isset($new_keys)) {
		      _db_create_keys($ret, $table, $new_keys);
	            }
}
/**
 * @} End of "ingroup schemaapi".
 */

