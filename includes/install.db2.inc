<?php
//DB2 database driver interface code for  0.0.1-beta version.

// DB2 specific install functions

/**
 * Check DB2 is available.
 *
 * @return
 *  TRUE/FALSE
 */
function db2_is_available() {
  if (extension_loaded('pdo_ibm') )
      return true;
  return false;
}

/**
 * Check if we can connect to db2
 *
 * @return
 *  TRUE/FALSE
 */
function drupal_test_db2($url, &$success) {
  if (!db2_is_available()) {
    drupal_set_message(st('PHP db2 support not enabled.'), 'error');
    return FALSE;
  }

  $url = parse_url($url);
  $conn_string = 'ibm:';

  // Decode url-encoded information in the db connection string
  if (isset($url['host'])) {
    $conn_string .= ' hostname='. urldecode($url['host']) . ';';
  }
  if (isset($url['path'])) {
    $conn_string .= ' database='. substr(urldecode($url['path']), 1) . ';';
  }
  if (isset($url['port'])) {
    $conn_string .= ' port='. urldecode($url['port']) . ';';
  }
  $conn_string .= ' protocol=TCPIP;EnableScrollableCursors=1;';
  // Test connecting to the database.
  $connection = null;
  try {
    $connection = new PDO($conn_string, urldecode($url['user']), urldecode($url['pass']) );
    $connection->setAttribute(PDO::ATTR_CASE, PDO::CASE_LOWER);
    $connection->setAttribute(PDO::ATTR_STRINGIFY_FETCHES, true);
    $connection->setAttribute(PDO::ATTR_PERSISTENT, true);
  } catch (PDOException $e) {
	  drupal_set_message(st('Failed to connect to your db2 database server. db2 reports the following message: %error.<ul><li>Are you sure you have the correct username and password?</li><li>Are you sure that you have typed the correct database hostname?</li><li>Are you sure that the database server is running?</li><li>Are you sure you typed the correct database name?</li></ul>For more help, see the <a href="http://drupal.org/node/258">Installation and upgrading handbook</a>. If you are unsure what these terms mean you should probably contact your hosting provider.', array('%error' => $e->getMessage() )), 'error');
    return FALSE;
  }

  $success = array('CONNECT');

  // Test CREATE.
  $query = 'CREATE TABLE drupal_install_test (id integer NOT NULL)';
  try {
    $result = $connection->exec($query);
  } catch (PDOException $e2) {
	  drupal_set_message(st('Failed to create a test table on your db2 database server with the command %query. db2 reports the following message: %error.<ul><li>Are you sure the configured username has the necessary db2 permissions to create tables in the database?</li></ul>For more help, see the <a href="http://drupal.org/node/258">Installation and upgrading handbook</a>. If you are unsure what these terms mean you should probably contact your hosting provider.', array('%query' => $query, '%error' => $e2->getMessage() )), 'error');
    return FALSE;
  }
  $err = FALSE;
  $success[] = 'SELECT';
  $success[] = 'CREATE';

  // Test INSERT.
  try {
    $query = 'INSERT INTO drupal_install_test (id) VALUES (1)';
    $result = $connection->exec($query);
    $success[] = 'INSERT';
    } catch (PDOException $e3) {
	   drupal_set_message(st('Failed to insert a value into a test table on your db2 database server. We tried inserting a value with the command %query db2 reported the following error: %error.', array('%query' => $query, '%error' => $e3->getMessage() )), 'error');
    $err = TRUE;
  }

  // Test UPDATE.
  try {
    $query = 'UPDATE drupal_install_test SET id = 2';
    $result = $connection->exec($query);
    $success[] = 'UPDATE';
  } catch (PDOException $e4) {
	  drupal_set_message(st('Failed to update a value in a test table on your db2 database server. We tried updating a value with the command %query and db2 reported the following error: %error.', array('%query' => $query, '%error' => $e4->getMessage() )), 'error');
    $err = TRUE;
  }

  // Test LOCK.
  try {
    $query = 'LOCK drupal_install_test IN EXCLUSIVE MODE';
    $result = $connection->exec($query);
    $success[] = 'LOCK';
  } catch (PDOException $e5) {
      drupal_set_message(st('Failed to lock a test table on your db2 database server. We tried locking a table with the command %query and db2 reported the following error: %error.', array('%query' => $query, '%error' => $e5->getMessage() )), 'error');
    $err = TRUE;
  }

  // Test UNLOCK, which is done automatically upon transaction end in PostgreSQL
  try {
    $query = 'COMMIT';
    $result = $connection->exec($query);
    $success[] = 'UNLOCK';
  } catch (PDOException $e6) {
    drupal_set_message(st('Failed to unlock a test table on your db2 database server. We tried unlocking a table with the command %query and db2 reported the following error: %error.', array('%query' => $query, '%error' => $e6->getMessage() )), 'error');
    $err = TRUE;
  }

  // Test DELETE.
  try {
    $query = 'DELETE FROM drupal_install_test';
    $result = $connection->exec($query);
    $success[] = 'DELETE';
  } catch (PDOException $e7) {
    drupal_set_message(st('Failed to delete a value from a test table on your db2 database server. We tried deleting a value with the command %query db2db2 reported the following error: %error.', array('%query' => $query, '%error' => $e7->getMessage() )), 'error');
    $err = TRUE;
  }

  // Test DROP.
  try {
    $query = 'DROP TABLE drupal_install_test';
    $result = $connection->exec($query);
    $success[] = 'DROP';
  } catch (PDOException $e8) {
    drupal_set_message(st('Failed to drop a test table from your db2 database server. We tried dropping a table with the command %query db2 reported the following error %error.', array('%query' => $query, '%error' => $e8->getMessage() )), 'error');
    $err = TRUE;
  }

  if ($err) {
    return FALSE;
  }
  return TRUE;
}
